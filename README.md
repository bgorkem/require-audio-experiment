## Audio Api Experiments ##

Using RequireJS AMD module downloader and Text plugin downloading base64 encoded mp3 and wav files to 
play them on a website. 

### Setup

* In the audio folder run convert.js to convert mp3 or wav files to their base64
encoded formats

* copy these files over to scripts folder

* require these files from AMD

```
define ([ 'text!./notif-wav.js',...], function(wavFile,..) {


}

```

* run the wav file by using  [ HTMLAudioElement]( https://developer.mozilla.org/en/docs/Web/API/HTMLAudioElement)

```
 var snd = new Audio("data:audio/mp3;wav," + wavFile);
     snd.play();
```   
