var connect = require('connect');

var open = require('open');

var serveStatic = require('serve-static');

connect().use(serveStatic(__dirname)).listen(8080, function(){
    console.log('Server running on 8080...');
    open('http://localhost:8080');
});