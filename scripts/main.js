define(['text!./notif-wav.js', 'text!./notif-mp3.js',], function (wav, mp3) {
  
  
    var snd = new Audio("data:audio/mp3;base64," + mp3);
    var snd2 = new Audio("data:audio/wav;base64," + wav);

   document.querySelector(".mp3").addEventListener('click', function () {
        snd.play();
    });

    document.querySelector(".wav").addEventListener('click', function () {
        snd2.play();
    });


});